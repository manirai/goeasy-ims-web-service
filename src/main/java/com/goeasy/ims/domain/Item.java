package com.goeasy.ims.domain;

import java.math.BigDecimal;

import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class Item {

	@Id
	private long id;
	private String name;
	private int quantity;
	private BigDecimal buyingRate;
	
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public int getQuantity() {
		return quantity;
	}
	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}
	public BigDecimal getBuyingRate() {
		return buyingRate;
	}
	public void setBuyingRate(BigDecimal buyingRate) {
		this.buyingRate = buyingRate;
	}
}